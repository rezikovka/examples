<?php

declare(strict_types=1);

namespace common\models\dataProviders;

use common\models\forms\userOption\UserOptionSearchForm;
use common\models\UserOption;

/**
 * Интерфейс провайдера данных для получения опций пользователя
 *
 * @author Vladimir Chalenko <rezikovka@gmail.com>
 */
interface UserOptionGeneralDataProviderInterface
{
    /**
     * Метод возвращает опции пользователя по переданному фильтру
     *
     * @param UserOptionSearchForm $userOptionSearchForm форма с фильтрами поиска опций
     * @return UserOption[]
     */
    public function getOptions(UserOptionSearchForm $userOptionSearchForm): array;
}
