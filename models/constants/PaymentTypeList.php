<?php

declare(strict_types=1);

namespace common\models\constants;

/**
 * Класс, хранящий в себе публичные константы типов платежных реквизитов пользователя
 *
 * @author Vladimir Chalenko <rezikovka@gmail.com>
 */
class PaymentTypeList
{
    /**
     * Тип платежных реквизитов "банковская карта"
     */
    public const USER_PAYMENT_CARD_REQUISITES_OPTION_NAME = 'userPaymentCardRequisites';
}
