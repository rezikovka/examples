<?php

declare(strict_types=1);

namespace common\models\storage;

use common\models\forms\userOption\UserOptionCreateForm;
use common\models\UserOption;
use LogicException;
use RuntimeException;

/**
 * Интерфейс менеджера хранилища данных опций пользователя
 *
 * @author Vladimir Chalenko <rezikovka@gmail.com>
 */
interface UserOptionGeneralStorageInterface
{
    /**
     * Метод создает новую запись опции пользователя и возвращает её идентификатор
     *
     * @param UserOptionCreateForm $createForm
     * @return int
     * @throws RuntimeException
     * @throws LogicException
     */
    public function addOption(UserOptionCreateForm $createForm): int;

    /**
     * Метод удаляет опцию пользователя
     *
     * @param UserOption $option
     * @throws RuntimeException
     */
    public function removeOption(UserOption $option): void;
}
