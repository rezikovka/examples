<?php

declare(strict_types=1);

namespace common\models\storage;

use common\models\forms\userOption\UserOptionCreateForm;
use common\models\UserOption;
use LogicException;
use RuntimeException;

/**
 * Сервис работы с хранилищем данных опций пользователя. Имплементация с использованием ActiveRecord
 *
 * @author Vladimir Chalenko <rezikovka@gmail.com>
 */
class UserOptionGeneralStorage implements UserOptionGeneralStorageInterface
{
    /**
     * @inheritDoc
     */
    public function addOption(UserOptionCreateForm $createForm): int
    {
        if (!$createForm->validate()) {
            throw new LogicException('Передана невалидная форма');
        }

        $option = new UserOption();
        $option->user_id = $createForm->userId;
        $option->option_name = $createForm->optionName;
        $option->option_value = $createForm->optionValue;

        if (!$option->save()) {
            throw new RuntimeException('Не удалось сохранить данные');
        }

        return $option->id;
    }

    /**
     * @inheritDoc
     */
    public function removeOption(UserOption $option): void
    {
        if (!$option->delete()) {
            throw new RuntimeException('Не удалось удалить данные');
        }
    }
}
