<?php

declare(strict_types=1);

namespace common\models\forms\userOption;

use yii\base\Model;

/**
 * Поисковая форма для провайдера данных опций пользователя
 *
 * @author Vladimir Chalenko <rezikovka@gmail.com>
 */
class UserOptionSearchForm extends Model
{
    /**
     * @var int идентификатор записи
     */
    public $id;

    /**
     * @var string название опции
     */
    public $optionName;

    /**
     * @var int идентификатор пользователя, которому пренадлежит опция
     */
    public $userId;

    /**
     * @var string значение опции
     */
    public $optionValue;

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['optionName', 'optionValue'], 'string'],
            [['userId', 'id'], 'integer'],
        ];
    }
}
