<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Модель данных записи опции профиля пользователя
 *
 * @property int $id
 * @property int $user_id
 * @property string $option_name
 * @property string $option_value
 *
 * @author Vladimir Chalenko <rezikovka@gmail.com>
 */
class UserOption extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%user_option}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['user_id', 'option_name', 'option_value'], 'required'],
            [['user_id'], 'integer'],
            [['option_name'], 'string', 'max' => 255],
            [['option_value'], 'string', 'max' => 8192],
        ];
    }
}
