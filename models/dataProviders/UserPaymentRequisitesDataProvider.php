<?php

declare(strict_types=1);

namespace common\models\dataProviders;

use common\models\constants\PaymentTypeList;
use common\models\forms\userOption\UserOptionSearchForm;
use common\models\User;
use common\models\UserOption;

/**
 * Провайдер данных платежных реквизитов пользователя
 *
 * @author Vladimir Chalenko <rezikovka@gmail.com>
 */
class UserPaymentRequisitesDataProvider
{
    /**
     * @var UserOptionGeneralDataProviderInterface
     */
    private $generalDataProvider;

    /**
     * @param UserOptionGeneralDataProviderInterface $generalDataProvider
     */
    public function __construct(UserOptionGeneralDataProviderInterface $generalDataProvider)
    {
        $this->generalDataProvider = $generalDataProvider;
    }

    /**
     * Метод возвращает список платежных карт пользователя
     *
     * @param User $user пользователь, для которого нужно сформировать список
     * @return UserOption[]
     */
    public function getPaymentCards(User $user): array
    {
        $searchForm = new UserOptionSearchForm();
        $searchForm->userId = $user->id;
        $searchForm->optionName = PaymentTypeList::USER_PAYMENT_CARD_REQUISITES_OPTION_NAME;

        $cards = $this->generalDataProvider->getOptions($searchForm);

        return $cards;
    }

    /**
     * Метод возвращает карту по идентификатору
     *
     * @param int $id идентификатор карты
     * @return UserOption|null
     */
    public function getPaymentCard(int $id): ?UserOption
    {
        $searchForm = new UserOptionSearchForm();
        $searchForm->id = $id;
        $searchForm->optionName = PaymentTypeList::USER_PAYMENT_CARD_REQUISITES_OPTION_NAME;

        $cards = $this->generalDataProvider->getOptions($searchForm);

        if (empty($cards)) {
            return null;
        }

        return $cards[0];
    }
}
