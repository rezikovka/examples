<?php

declare(strict_types=1);

namespace common\models\dataProviders;

use common\models\forms\userOption\UserOptionSearchForm;
use common\models\UserOption;
use LogicException;
use Yii;

/**
 * Провайдер данных для получения опций пользователя
 *
 * @author Vladimir Chalenko <rezikovka@gmail.com>
 */
class UserOptionGeneralDataProvider implements UserOptionGeneralDataProviderInterface
{
    /**
     * @inheritDoc
     */
    public function getOptions(UserOptionSearchForm $userOptionSearchForm): array
    {
        if (!$userOptionSearchForm->validate()) {
            throw new LogicException(Yii::t('private', 'Передана невалидная форма'));
        }

        $query = UserOption::find();

        $models = $query->andFilterWhere(['option_name' => $userOptionSearchForm->optionName])
            ->andFilterWhere(['id' => $userOptionSearchForm->id])
            ->andFilterWhere(['user_id' => $userOptionSearchForm->userId])
            ->andFilterWhere(['option_value' => $userOptionSearchForm->optionValue])
            ->all();

        return $models;
    }
}
