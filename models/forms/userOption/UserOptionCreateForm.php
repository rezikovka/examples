<?php

declare(strict_types=1);

namespace common\models\forms\userOption;

use yii\base\Model;

/**
 * Форма создания новой опции пользователя
 *
 * @author Vladimir Chalenko <rezikovka@gmail.com>
 */
class UserOptionCreateForm extends Model
{
    /**
     * @var string название опции
     */
    public $optionName;

    /**
     * @var int идентификатор пользователя, которому пренадлежит опция
     */
    public $userId;

    /**
     * @var string значение опции
     */
    public $optionValue;

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['optionName', 'optionValue', 'userId'], 'required'],
            [['optionName', 'optionValue'], 'string'],
            [['userId'], 'integer'],
        ];
    }
}
