<?php

declare(strict_types=1);

namespace common\models\storage;

use common\models\constants\PaymentTypeList;
use common\models\forms\paymentRequisites\CardPaymentRequisitesForm;
use common\models\forms\userOption\UserOptionCreateForm;
use common\models\User;
use common\models\UserOption;
use LogicException;

/**
 * Класс управления хранилищем платежных реквизитов пользователя
 *
 * @author Vladimir Chalenko <rezikovka@gmail.com>
 */
class UserPaymentRequisitesStorage
{
    /**
     * @var UserOptionGeneralStorageInterface
     */
    private $userOptionGeneralStorage;

    /**
     * @param UserOptionGeneralStorageInterface $userOptionGeneralStorage
     */
    public function __construct(UserOptionGeneralStorageInterface $userOptionGeneralStorage)
    {
        $this->userOptionGeneralStorage = $userOptionGeneralStorage;
    }

    /**
     * Метод добавляет запись с номером платежной карты пользователя и возвращает её идентификатор
     *
     * @param CardPaymentRequisitesForm $cardPaymentRequisitesForm данные записи
     * @param User $user пользователь, которому присваивается запись
     * @throws LogicException
     *
     * @return int
     */
    public function createCard(CardPaymentRequisitesForm $cardPaymentRequisitesForm, User $user): int
    {
        if (!$cardPaymentRequisitesForm->validate()) {
            throw new LogicException('Передана невалидная форма');
        }

        $createForm = new UserOptionCreateForm();
        $createForm->optionName = PaymentTypeList::USER_PAYMENT_CARD_REQUISITES_OPTION_NAME;
        $createForm->optionValue = $cardPaymentRequisitesForm->cardNumber;
        $createForm->userId = $user->id;

        return $this->userOptionGeneralStorage->addOption($createForm);
    }

    /**
     * Метод удаляет запись с номером платежной карты пользователя
     *
     * @param UserOption $card
     */
    public function deleteCard(UserOption $card): void
    {
        if ($card->option_name !== PaymentTypeList::USER_PAYMENT_CARD_REQUISITES_OPTION_NAME) {
            throw new LogicException('Передан некорректный объект');
        }

        $this->userOptionGeneralStorage->removeOption($card);
    }
}
