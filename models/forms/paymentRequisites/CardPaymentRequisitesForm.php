<?php

declare(strict_types=1);

namespace common\models\forms\paymentRequisites;

use Yii;
use yii\base\Model;

/**
 * Форма реквизитов платежной карты для осуществления выплат
 *
 * @author Vladimir Chalenko <rezikovka@gmail.com>
 */
class CardPaymentRequisitesForm extends Model
{
    /**
     * @var string
     */
    public $cardNumber;

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['cardNumber'], 'required'],
            [['cardNumber'], 'string'],
            [['cardNumber'], 'match', 'pattern' => '/^\d{4}\s*\d{4}\s*\d{4}\s*\d{4}$/'],
            [
                ['cardNumber'],
                'filter',
                'filter' => function (string $value): string {
                    return preg_replace('/\s/', '', $value);
                },
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels(): array
    {
        return [
            'cardNumber' => Yii::t('public', 'Номер карты'),
        ];
    }
}
